{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h1>Dark solitons in holographic superfluids</h1>\n",
    "<div>\n",
    "<p>\n",
    "Victor Santos<br/>\n",
    "victor_santos@fisica.ufc.br<br/>\n",
    "8 April 2017\n",
    "</p>\n",
    "<p>\n",
    "In this worksheet I derive the equations of motion for the holographic superfluid, as discussed in the <a href=\"https://arxiv.org/pdf/0911.1866.pdf#section.2\">Section 2</a> of <a href=\"https://arxiv.org/abs/0911.1866\">Phys. Rev. D <b>81</b> 126011 (2010)</a>.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "reset()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "%display latex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h2>Description</h2>\n",
    "<p>\n",
    "The holographic model for a superfluid system was constructed from the lagrangian density\n",
    "\\begin{equation}\n",
    "L = L_g + L_H,\n",
    "\\end{equation}\n",
    "where $L = 1/(2\\kappa^2)(R - 12\\Lambda)$ is the Einstein-Hilbert lagrangian density and $L_H$ is the lagrangian density of scalar electrodynamics:\n",
    "\\begin{equation}\n",
    "L_H = \\frac{1}{q^2}\\bigg[-\\frac{1}{4}F^2 - \\big(D_{a}\\phi \\bar{D}^{a}\\bar{\\phi} + V(\\phi\\bar{\\phi})\\big)\\bigg],\n",
    "\\end{equation}\n",
    "where the fields were rescaled such that the coupling constant appears multiplying the action.\n",
    "</p>\n",
    "\n",
    "<h3>Assumptions</h3>\n",
    "<p>\n",
    "<ol>\n",
    "<li>Probe limit: take $\\kappa/q\\to 0$ with $\\phi$ and $A$ finite. This evade the problem of solving Einstein equation, aloowing to treat the fields as they were in a non-dynamical gravitational background.</li>\n",
    "<li>Gauge fixing: assume $A_z = 0$, which leads to $A_x = 0$ and $\\phi$ real</li>\n",
    "<li>Translational invariance: assume $A_y = 0$</li>\n",
    "</ol>\n",
    "Moreover, we look for <i>static solutions</i> (no time dependence). The translational invariance also implies that the fields do not depend on $y$. therefore, all fields depend on $(z,x)$ only.\n",
    "</p>\n",
    "<p>\n",
    "The equations of motion for $L_H$ will be then\n",
    "\\begin{align}\n",
    "D^{a}D_{a}\\phi + V'(\\phi^2)\\,\\phi &= 0,\\tag{Klein-Gordon eq.}\\\\\n",
    "\\nabla^b F_{ab} &= J_a,\\ J_a = -\\mathrm{i}(\\phi D_{a}\\phi - \\phi\\bar{D}_{a}\\phi),\\tag{Maxwell eq.}\\\\\n",
    "\\nabla_{[a}F_{bc]} &= 0.\\tag{Bianchi identity}\n",
    "\\end{align}\n",
    "</p>\n",
    "\n",
    "<p>\n",
    "For the background, we assume a 4D planar AdS black hole with in Poincaré coordinates:\n",
    "\\begin{equation}\n",
    "g = L^2\\bigg(-\\frac{f(z)}{z^2}{\\mathrm{d}t}^2 + \\frac{{\\mathrm{d}z}^2}{z^2\\,f(z)} + \\frac{1}{z^2}{\\mathrm{d}\\vec{x}}^2\\bigg),\\quad f(z) = 1 - {\\bigg(\\frac{z}{z_T}\\bigg)}^3.\n",
    "\\end{equation}\n",
    "The Hawking temperature of the black hole, $T_H=3/(4\\pi z_T)$, is identified with the equilibrium temperature of the dual field theory. With no loss of generality we can assume $z_T = 1$.\n",
    "</p>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h2>Derivation of the Equations of motion</h2>\n",
    "<h3>Background</h3>\n",
    "<p>\n",
    "We first define the ambient manifold, and the coordinate patch:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left({AdS_{4}},(t, z, x, y)\\right)</script></html>"
      ],
      "text/plain": [
       "Chart (M, (t, z, x, y))"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "M = Manifold(4, 'M', r'{AdS_{4}}')\n",
    "PoincareCoord.<t,z,x,y> = M.chart(r't z:[0,1) x:(-oo,oo) y:(-oo,oo)')\n",
    "PoincareCoord"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Now we define the metric and compute its inverse:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}g = -\\frac{f\\left(z\\right)}{z^{2}} \\mathrm{d} t\\otimes \\mathrm{d} t + \\frac{1}{z^{2} f\\left(z\\right)} \\mathrm{d} z\\otimes \\mathrm{d} z + \\frac{1}{z^{2}} \\mathrm{d} x\\otimes \\mathrm{d} x + \\frac{1}{z^{2}} \\mathrm{d} y\\otimes \\mathrm{d} y</script></html>"
      ],
      "text/plain": [
       "g = -f(z)/z^2 dt*dt + 1/(z^2*f(z)) dz*dz + z^(-2) dx*dx + z^(-2) dy*dy"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "g = M.lorentzian_metric('g')\n",
    "f = M.scalar_field(function('f')(z), name='f')\n",
    "\n",
    "g[0,0] = -f/z^2\n",
    "g[1,1] = 1/(z^2 * f)\n",
    "g[2,2] = 1/z^2\n",
    "g[3,3] = 1/z^2\n",
    "\n",
    "g.display()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-\\frac{z^{2}}{f\\left(z\\right)} \\frac{\\partial}{\\partial t }\\otimes \\frac{\\partial}{\\partial t } + z^{2} f\\left(z\\right) \\frac{\\partial}{\\partial z }\\otimes \\frac{\\partial}{\\partial z } + z^{2} \\frac{\\partial}{\\partial x }\\otimes \\frac{\\partial}{\\partial x } + z^{2} \\frac{\\partial}{\\partial y }\\otimes \\frac{\\partial}{\\partial y }</script></html>"
      ],
      "text/plain": [
       "-z^2/f(z) d/dt*d/dt + z^2*f(z) d/dz*d/dz + z^2 d/dx*d/dx + z^2 d/dy*d/dy"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ginv = g.up(g,0).up(g,1)\n",
    "ginv.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We also need to compute the compatible connection for the metric:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\nabla_{g}</script></html>"
      ],
      "text/plain": [
       "Levi-Civita connection nabla_g associated with the Lorentzian metric g on the 4-dimensional differentiable manifold M"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nabla = g.connection()\n",
    "nabla"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "One can check that the Ricci scalar for this spacetime is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\begin{array}{llcl} \\mathrm{r}\\left(g\\right):& {AdS_{4}} & \\longrightarrow & \\mathbb{R} \\\\ & \\left(t, z, x, y\\right) & \\longmapsto & -z^{2} \\frac{\\partial^2\\,f}{\\partial z ^ 2} + 6 \\, z \\frac{\\partial\\,f}{\\partial z} - 12 \\, f\\left(z\\right) \\end{array}</script></html>"
      ],
      "text/plain": [
       "r(g): M --> R\n",
       "   (t, z, x, y) |--> -z^2*d^2(f)/dz^2 + 6*z*d(f)/dz - 12*f(z)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RicciScalar = g.ricci_scalar()\n",
    "RicciScalar.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "which is constant for $f(z) = 1 - z^3$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-12</script></html>"
      ],
      "text/plain": [
       "-12"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tmp = 1 - z^3\n",
    "-z^2*(diff(tmp,z,2)) + 6*z*diff(tmp,z) - 12*tmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h3>Equations of motion</h3>\n",
    "<p>\n",
    "We first define the vector and scalar fields:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "A = M.one_form()\n",
    "phi = M.scalar_field(function('phi')(z,x), name='{\\phi}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "and now we set the components of the vector field and compute the Faraday tensor $F = \\mathrm{d}A$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}A_{0}\\left(z, x\\right) \\mathrm{d} t</script></html>"
      ],
      "text/plain": [
       "A0(z, x) dt"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Keranen \n",
    "A[0] = M.scalar_field(function('A0')(z,x))\n",
    "\n",
    "# This is for the general case\n",
    "#AComponents = [M.scalar_field(function('A'+str(i))(t,z,x,y), name='{A_{'+str(i)+'}}') for i in range(4)]\n",
    "#for i in range(4):\n",
    "#    A[i] = AComponents[i]\n",
    "\n",
    "A.display()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left(\\begin{array}{rrrr}\n",
       "0 & -\\frac{\\partial}{\\partial z}A_{0}\\left(z, x\\right) & -\\frac{\\partial}{\\partial x}A_{0}\\left(z, x\\right) & 0 \\\\\n",
       "\\frac{\\partial}{\\partial z}A_{0}\\left(z, x\\right) & 0 & 0 & 0 \\\\\n",
       "\\frac{\\partial}{\\partial x}A_{0}\\left(z, x\\right) & 0 & 0 & 0 \\\\\n",
       "0 & 0 & 0 & 0\n",
       "\\end{array}\\right)</script></html>"
      ],
      "text/plain": [
       "[                 0 -diff(A0(z, x), z) -diff(A0(z, x), x)                  0]\n",
       "[ diff(A0(z, x), z)                  0                  0                  0]\n",
       "[ diff(A0(z, x), x)                  0                  0                  0]\n",
       "[                 0                  0                  0                  0]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "F = A.exterior_derivative()\n",
    "F[:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h4>Scalar field</h4>\n",
    "<p>\n",
    "To compute the equation of motion for the scalar field we need to compute the gauge covariant derivative\n",
    "\\begin{equation}\n",
    "D_{a}\\phi = \\nabla_{a}\\phi - \\mathrm{i} A_{a}\\phi,\n",
    "\\end{equation}\n",
    "as well as the second derivatives $D_{a}D_{b}\\phi$:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "def gauge_covariant_derivative (expr):\n",
    "    '''\n",
    "    Computes the gauge covariant derivative of `expr'\n",
    "    '''\n",
    "    return nabla(expr) - I*A*expr\n",
    "\n",
    "# First derivative\n",
    "Dphi = gauge_covariant_derivative(phi)\n",
    "Dphi.set_name('{(D_{a}{\\phi})}')\n",
    "\n",
    "# Second derivative\n",
    "D2phi = gauge_covariant_derivative(Dphi)\n",
    "D2phi.set_name('{(D_{a}D_{b}{\\phi})}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "With the term $D_{a}D_{b}\\phi$, we compute the gauge covariant laplacian $D^{a}D_{a}\\phi = g^{ab}D_{a}D_{b}\\phi$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "# I'm not sure this is the best way to do it!\n",
    "CovariantLaplacianPhi = (ginv*D2phi)['^{ab}_{ac}']\n",
    "CovariantLaplacianPhi = CovariantLaplacianPhi['^{a}_{a}']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "For an arbitrary scalar potential we simply define a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}{{V^{\\prime}({\\phi}^2)}}</script></html>"
      ],
      "text/plain": [
       "Vp"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Vp = var('Vp', latex_name='{V^{\\prime}({\\phi}^2)}')\n",
    "Vp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "With all terms we need, now we compute the Klein-Gordon equation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{{V^{\\prime}({\\phi}^2)}} \\phi\\left(z, x\\right) + \\frac{z^{2} A_{0}\\left(z, x\\right)^{2} \\phi\\left(z, x\\right) + z^{2} f\\left(z\\right)^{2} \\frac{\\partial^{2}}{(\\partial z)^{2}}\\phi\\left(z, x\\right) + z^{2} f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial x)^{2}}\\phi\\left(z, x\\right) + {\\left(z^{2} f\\left(z\\right) \\frac{\\partial}{\\partial z}f\\left(z\\right) - 2 \\, z f\\left(z\\right)^{2}\\right)} \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right)}{f\\left(z\\right)} = 0</script></html>"
      ],
      "text/plain": [
       "-Vp*phi(z, x) + (z^2*A0(z, x)^2*phi(z, x) + z^2*f(z)^2*diff(phi(z, x), z, z) + z^2*f(z)*diff(phi(z, x), x, x) + (z^2*f(z)*diff(f(z), z) - 2*z*f(z)^2)*diff(phi(z, x), z))/f(z) == 0"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_scalar_field = CovariantLaplacianPhi.expr() - Vp*phi.expr() == 0\n",
    "EOM_scalar_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We see that the term $f(z)$ is common to several terms, so let us expand the expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{z^{2} A_{0}\\left(z, x\\right)^{2} \\phi\\left(z, x\\right)}{f\\left(z\\right)} + z^{2} \\frac{\\partial}{\\partial z}f\\left(z\\right) \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right) + z^{2} f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}\\phi\\left(z, x\\right) - 2 \\, z f\\left(z\\right) \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right) + z^{2} \\frac{\\partial^{2}}{(\\partial x)^{2}}\\phi\\left(z, x\\right) - {{V^{\\prime}({\\phi}^2)}} \\phi\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "z^2*A0(z, x)^2*phi(z, x)/f(z) + z^2*diff(f(z), z)*diff(phi(z, x), z) + z^2*f(z)*diff(phi(z, x), z, z) - 2*z*f(z)*diff(phi(z, x), z) + z^2*diff(phi(z, x), x, x) - Vp*phi(z, x) == 0"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_scalar_field.expand()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Also, we can see that most of the terms contain a $z^2$ term. We can therefore factor it out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{A_{0}\\left(z, x\\right)^{2} \\phi\\left(z, x\\right)}{f\\left(z\\right)} + \\frac{\\partial}{\\partial z}f\\left(z\\right) \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right) + f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}\\phi\\left(z, x\\right) - \\frac{2 \\, f\\left(z\\right) \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right)}{z} - \\frac{{{V^{\\prime}({\\phi}^2)}} \\phi\\left(z, x\\right)}{z^{2}} + \\frac{\\partial^{2}}{(\\partial x)^{2}}\\phi\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "A0(z, x)^2*phi(z, x)/f(z) + diff(f(z), z)*diff(phi(z, x), z) + f(z)*diff(phi(z, x), z, z) - 2*f(z)*diff(phi(z, x), z)/z - Vp*phi(z, x)/z^2 + diff(phi(z, x), x, x) == 0"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(EOM_scalar_field/z^2).expand()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "and then we obtain the simplified for the equation of motion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "EOM_scalar_field = (EOM_scalar_field/z^2).expand()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Now, we can ground similar terms and collect the coeffients of the derivatives, to obtain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}{\\left(\\frac{A_{0}\\left(z, x\\right)^{2}}{f\\left(z\\right)} - \\frac{{{V^{\\prime}({\\phi}^2)}}}{z^{2}}\\right)} \\phi\\left(z, x\\right) - {\\left(\\frac{2 \\, f\\left(z\\right)}{z} - \\frac{\\partial}{\\partial z}f\\left(z\\right)\\right)} \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right) + f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}\\phi\\left(z, x\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}\\phi\\left(z, x\\right)</script></html>"
      ],
      "text/plain": [
       "(A0(z, x)^2/f(z) - Vp/z^2)*phi(z, x) - (2*f(z)/z - diff(f(z), z))*diff(phi(z, x), z) + f(z)*diff(phi(z, x), z, z) + diff(phi(z, x), x, x)"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tmp = EOM_scalar_field.lhs()\n",
    "s = 0\n",
    "for i in range(3):\n",
    "    c = tmp.coefficient(diff(phi.expr(), z, i)).expand()\n",
    "    d = tmp.coefficient(diff(phi.expr(), x, i)).expand()\n",
    "    s += c*diff(phi.expr(), z, i) + d*diff(phi.expr(), x, i)\n",
    "    if i == 0:\n",
    "        s -= c*diff(phi.expr(), z, i)\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "which is finally the equation of motion for the scalar field:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "EOM_scalar_field = s == 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h3>Equation of motion for the vector field</h3>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We first compute the 1-form current (rhs of Maxwell equation):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-A_{0}\\left(z, x\\right) \\phi\\left(z, x\\right)^{2} \\mathrm{d} t</script></html>"
      ],
      "text/plain": [
       "-A0(z, x)*phi(z, x)^2 dt"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def make_J(A, phi):\n",
    "    '''\n",
    "    Creates a time-INDEPENDENT current as a 1-form\n",
    "    '''\n",
    "    Dphi = nabla(phi) - I*A*phi\n",
    "    DBarPhi = nabla(phi) + I*A*phi\n",
    "    \n",
    "    J = -I*(1/2)*(phi*Dphi - phi*DBarPhi)\n",
    "    \n",
    "    return J\n",
    "\n",
    "J = make_J(A, phi)\n",
    "J.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "the first pair of Maxwell's equations, $\\nabla^aF_{ab} = J_a$, can be written in the differential forms language as\n",
    "$\\ast\\mathrm{d}\\ast F = J$, which is nicely handled by <a href=\"http://sagemanifolds.obspm.fr/\">SageManifolds</a>. Computing the rhs of this equation,\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left( -z^{2} f\\left(z\\right) \\frac{\\partial^2\\,A_{0}}{\\partial z ^ 2} - z^{2} \\frac{\\partial^2\\,A_{0}}{\\partial x ^ 2} \\right) \\mathrm{d} t</script></html>"
      ],
      "text/plain": [
       "(-z^2*f(z)*d^2(A0)/dz^2 - z^2*d^2(A0)/dx^2) dt"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_vector_field_LHS = F.hodge_dual(g).exterior_derivative().hodge_dual(g)\n",
    "EOM_vector_field_LHS.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "we see that there is in fact only one equation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-z^{2} f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}A_{0}\\left(z, x\\right) + A_{0}\\left(z, x\\right) \\phi\\left(z, x\\right)^{2} - z^{2} \\frac{\\partial^{2}}{(\\partial x)^{2}}A_{0}\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "-z^2*f(z)*diff(A0(z, x), z, z) + A0(z, x)*phi(z, x)^2 - z^2*diff(A0(z, x), x, x) == 0"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_vector_field = EOM_vector_field_LHS[0].expr() - J[0].expr() == 0\n",
    "EOM_vector_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We can factor out the term $z^2$ to obtain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}A_{0}\\left(z, x\\right) - \\frac{A_{0}\\left(z, x\\right) \\phi\\left(z, x\\right)^{2}}{z^{2}} + \\frac{\\partial^{2}}{(\\partial x)^{2}}A_{0}\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "f(z)*diff(A0(z, x), z, z) - A0(z, x)*phi(z, x)^2/z^2 + diff(A0(z, x), x, x) == 0"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_vector_field = (-EOM_vector_field/z^2).expand()\n",
    "EOM_vector_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "The Bianchi identity $\\mathrm{d}F = 0$ however, is trivially satisfied for this ansatz:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}0</script></html>"
      ],
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "F.exterior_derivative().display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "and then we have no more equations. Therefore, the equations of motion are\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}{\\left(\\frac{A_{0}\\left(z, x\\right)^{2}}{f\\left(z\\right)} - \\frac{{{V^{\\prime}({\\phi}^2)}}}{z^{2}}\\right)} \\phi\\left(z, x\\right) - {\\left(\\frac{2 \\, f\\left(z\\right)}{z} - \\frac{\\partial}{\\partial z}f\\left(z\\right)\\right)} \\frac{\\partial}{\\partial z}\\phi\\left(z, x\\right) + f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}\\phi\\left(z, x\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}\\phi\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "(A0(z, x)^2/f(z) - Vp/z^2)*phi(z, x) - (2*f(z)/z - diff(f(z), z))*diff(phi(z, x), z) + f(z)*diff(phi(z, x), z, z) + diff(phi(z, x), x, x) == 0"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_scalar_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "for the scalar field, and"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}A_{0}\\left(z, x\\right) - \\frac{A_{0}\\left(z, x\\right) \\phi\\left(z, x\\right)^{2}}{z^{2}} + \\frac{\\partial^{2}}{(\\partial x)^{2}}A_{0}\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "f(z)*diff(A0(z, x), z, z) - A0(z, x)*phi(z, x)^2/z^2 + diff(A0(z, x), x, x) == 0"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_vector_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "for the vector field.\n",
    "</p>\n",
    "<p>\n",
    "In the paper, the authors make a redefinition in the scalar field, defining\n",
    "\\begin{equation}\n",
    "\\phi(z,x) = \\frac{z}{\\sqrt{2}}R(z,x).\n",
    "\\end{equation}\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "R = M.scalar_field(function('R')(z,x), name='R')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "With this redefinition, the equation of motion for the scalar field becomes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\sqrt{2} z A_{0}\\left(z, x\\right)^{2} R\\left(z, x\\right)}{2 \\, f\\left(z\\right)} + \\frac{1}{2} \\, \\sqrt{2} z f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}R\\left(z, x\\right) + \\frac{1}{2} \\, \\sqrt{2} z \\frac{\\partial}{\\partial z}R\\left(z, x\\right) \\frac{\\partial}{\\partial z}f\\left(z\\right) + \\frac{1}{2} \\, \\sqrt{2} z \\frac{\\partial^{2}}{(\\partial x)^{2}}R\\left(z, x\\right) + \\frac{1}{2} \\, \\sqrt{2} R\\left(z, x\\right) \\frac{\\partial}{\\partial z}f\\left(z\\right) - \\frac{\\sqrt{2} {{V^{\\prime}({\\phi}^2)}} R\\left(z, x\\right)}{2 \\, z} - \\frac{\\sqrt{2} R\\left(z, x\\right) f\\left(z\\right)}{z}</script></html>"
      ],
      "text/plain": [
       "1/2*sqrt(2)*z*A0(z, x)^2*R(z, x)/f(z) + 1/2*sqrt(2)*z*f(z)*diff(R(z, x), z, z) + 1/2*sqrt(2)*z*diff(R(z, x), z)*diff(f(z), z) + 1/2*sqrt(2)*z*diff(R(z, x), x, x) + 1/2*sqrt(2)*R(z, x)*diff(f(z), z) - 1/2*sqrt(2)*Vp*R(z, x)/z - sqrt(2)*R(z, x)*f(z)/z"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tmp = EOM_scalar_field.lhs()\n",
    "s = 0\n",
    "for i in range(3):\n",
    "    c = tmp.coefficient(diff(phi.expr(), z, i)).expand()\n",
    "    d = tmp.coefficient(diff(phi.expr(), x, i)).expand()\n",
    "    s += c*diff(z*R.expr()/sqrt(2), z, i) + d*diff(z*R.expr()/sqrt(2), x, i)\n",
    "    if i == 0:\n",
    "        s -= c*diff(z*R.expr()/sqrt(2), z, i)\n",
    "s = s.expand()\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "and then we factor out the term $z/\\sqrt(2)$ to obtain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{A_{0}\\left(z, x\\right)^{2} R\\left(z, x\\right)}{f\\left(z\\right)} + f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}R\\left(z, x\\right) + \\frac{\\partial}{\\partial z}R\\left(z, x\\right) \\frac{\\partial}{\\partial z}f\\left(z\\right) + \\frac{R\\left(z, x\\right) \\frac{\\partial}{\\partial z}f\\left(z\\right)}{z} - \\frac{{{V^{\\prime}({\\phi}^2)}} R\\left(z, x\\right)}{z^{2}} - \\frac{2 \\, R\\left(z, x\\right) f\\left(z\\right)}{z^{2}} + \\frac{\\partial^{2}}{(\\partial x)^{2}}R\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "A0(z, x)^2*R(z, x)/f(z) + f(z)*diff(R(z, x), z, z) + diff(R(z, x), z)*diff(f(z), z) + R(z, x)*diff(f(z), z)/z - Vp*R(z, x)/z^2 - 2*R(z, x)*f(z)/z^2 + diff(R(z, x), x, x) == 0"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_scalar_field = s == 0 \n",
    "EOM_scalar_field = (sqrt(2)*EOM_scalar_field/z).expand()\n",
    "EOM_scalar_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "We finally collect similar terms and then we have"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}{\\left(\\frac{A_{0}\\left(z, x\\right)^{2}}{f\\left(z\\right)} + \\frac{\\frac{\\partial}{\\partial z}f\\left(z\\right)}{z} - \\frac{{{V^{\\prime}({\\phi}^2)}}}{z^{2}} - \\frac{2 \\, f\\left(z\\right)}{z^{2}}\\right)} R\\left(z, x\\right) + f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}R\\left(z, x\\right) + \\frac{\\partial}{\\partial z}R\\left(z, x\\right) \\frac{\\partial}{\\partial z}f\\left(z\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}R\\left(z, x\\right)</script></html>"
      ],
      "text/plain": [
       "(A0(z, x)^2/f(z) + diff(f(z), z)/z - Vp/z^2 - 2*f(z)/z^2)*R(z, x) + f(z)*diff(R(z, x), z, z) + diff(R(z, x), z)*diff(f(z), z) + diff(R(z, x), x, x)"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tmp = EOM_scalar_field.lhs()\n",
    "s = 0\n",
    "for i in range(3):\n",
    "    c = tmp.coefficient(diff(R.expr(), z, i)).expand()\n",
    "    d = tmp.coefficient(diff(R.expr(), x, i)).expand()\n",
    "    s += c*diff(R.expr(), z, i) + d*diff(R.expr(), x, i)\n",
    "    if i == 0:\n",
    "        s -= c*diff(R.expr(), z, i)\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "which is in agreement with eq. (7) of the paper, if we take $V(\\phi^2) = -2\\phi^2$. Therefore we can set it as our equation of motion:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "EOM_scalar_field = s == 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "For the vector field equation we don't have any trouble doing the field redefinition:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-\\frac{1}{2} \\, A_{0}\\left(z, x\\right) R\\left(z, x\\right)^{2} + f\\left(z\\right) \\frac{\\partial^{2}}{(\\partial z)^{2}}A_{0}\\left(z, x\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}A_{0}\\left(z, x\\right) = 0</script></html>"
      ],
      "text/plain": [
       "-1/2*A0(z, x)*R(z, x)^2 + f(z)*diff(A0(z, x), z, z) + diff(A0(z, x), x, x) == 0"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "EOM_vector_field = EOM_vector_field.subs({phi.expr():z*R.expr()/sqrt(2)})\n",
    "EOM_vector_field"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "which agree with eq. (8) of the paper, except by the first term, which differs by a factor of $1/2$. Probably the authors forgot the include this factor after the field redefinition."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 7.6",
   "language": "",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
