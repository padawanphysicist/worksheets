{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h1>Klein-Gordon equation in static, spherically symmetric spacetimes</h1>\n",
    "<div>\n",
    "<p>\n",
    "Victor Santos<br/>\n",
    "victor_santos@fisica.ufc.br<br/>\n",
    "8 May 2017\n",
    "</p>\n",
    "<p>\n",
    "In this worksheet I derive the equation for a scalar field in a static, spherically symmetric spacetime background.\n",
    "</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "reset()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p>\n",
    "First we set up the notebook to display mathematical objects using LaTeX formatting:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "%display latex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h2>Static, spherically symmetric spacetime</h2>\n",
    "<p>\n",
    "We declare the spacetime $M$ as a 4-dimensional manifold:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4-dimensional differentiable manifold M\n"
     ]
    }
   ],
   "source": [
    "M = Manifold(4, 'M')\n",
    "print(M)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p>\n",
    "and then introduce a coordinate system on $M$:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left(M,(t, r, {\\theta}, {\\phi})\\right)</script></html>"
      ],
      "text/plain": [
       "Chart (M, (t, r, th, ph))"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "coord.<t,r,th,ph> = M.chart(r't r:[0,+oo) th:[0,pi]:\\theta ph:[0,2*pi):\\phi')\n",
    "coord"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "The metric for a static, spherically symmetric spacetime can be written as\n",
    "\\begin{equation}\n",
    "{\\mathrm{d}s}^2 = -f(r){\\mathrm{d}t}^2 + \\frac{{\\mathrm{d}r}^2}{f(r)} + r^2{\\mathrm{d}\\Omega}^2,\n",
    "\\end{equation}\n",
    "where $f(r)$ is an arbitrary function and ${\\mathrm{d}\\Omega}^2$ is the metric of the unit 2-sphere.\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}g = -f\\left(r\\right) \\mathrm{d} t\\otimes \\mathrm{d} t + \\frac{1}{f\\left(r\\right)} \\mathrm{d} r\\otimes \\mathrm{d} r + r^{2} \\mathrm{d} {\\theta}\\otimes \\mathrm{d} {\\theta} + r^{2} \\sin\\left({\\theta}\\right)^{2} \\mathrm{d} {\\phi}\\otimes \\mathrm{d} {\\phi}</script></html>"
      ],
      "text/plain": [
       "g = -f(r) dt*dt + 1/f(r) dr*dr + r^2 dth*dth + r^2*sin(th)^2 dph*dph"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "f = M.scalar_field(function('f')(r), name='f')\n",
    "\n",
    "g = M.lorentzian_metric('g')\n",
    "g[0,0] = -f\n",
    "g[1,1] = 1/f\n",
    "g[2,2] = r^2\n",
    "g[3,3] = (r*sin(th))^2\n",
    "g.display()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p>\n",
    "We also need to define a connection:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\nabla_{g}</script></html>"
      ],
      "text/plain": [
       "Levi-Civita connection nabla_g associated with the Lorentzian metric g on the 4-dimensional differentiable manifold M"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "nabla = g.connection()\n",
    "nabla"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h2>Klein-Gordon equation</h2>\n",
    "<p>\n",
    "The Klein-Gordon equation is\n",
    "\\begin{equation}\n",
    "\\nabla^a\\nabla_a\\phi - m^2\\phi = 0,\n",
    "\\end{equation}\n",
    "where $m$ is the mass of the scalar field $\\phi$.\n",
    "</p>\n",
    "<p>\n",
    "We use the following decomposition for the scalar field:\n",
    "\\begin{equation}\n",
    "\\phi(t,r,\\theta,\\phi) = \\frac{1}{r}\\psi(t,r)Y(\\theta,\\phi)\n",
    "\\end{equation}\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\cos\\left({\\theta}\\right) f\\left(r\\right) \\psi\\left(t, r\\right) \\sin\\left({\\theta}\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right) + f\\left(r\\right) \\psi\\left(t, r\\right) \\sin\\left({\\theta}\\right)^{2} \\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right) + {\\left(r^{2} f\\left(r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right) \\frac{\\partial}{\\partial r}\\psi\\left(t, r\\right) + r^{2} f\\left(r\\right)^{2} \\frac{\\partial^{2}}{(\\partial r)^{2}}\\psi\\left(t, r\\right) - r^{2} \\frac{\\partial^{2}}{(\\partial t)^{2}}\\psi\\left(t, r\\right) - {\\left(m^{2} r^{2} f\\left(r\\right) + r f\\left(r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)\\right)} \\psi\\left(t, r\\right)\\right)} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)^{2} + f\\left(r\\right) \\psi\\left(t, r\\right) \\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} f\\left(r\\right) \\sin\\left({\\theta}\\right)^{2}}</script></html>"
      ],
      "text/plain": [
       "(cos(th)*f(r)*psi(t, r)*sin(th)*diff(Y(th, ph), th) + f(r)*psi(t, r)*sin(th)^2*diff(Y(th, ph), th, th) + (r^2*f(r)*diff(f(r), r)*diff(psi(t, r), r) + r^2*f(r)^2*diff(psi(t, r), r, r) - r^2*diff(psi(t, r), t, t) - (m^2*r^2*f(r) + r*f(r)*diff(f(r), r))*psi(t, r))*Y(th, ph)*sin(th)^2 + f(r)*psi(t, r)*diff(Y(th, ph), ph, ph))/(r^3*f(r)*sin(th)^2)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Mass of the scalar field\n",
    "m = var('m', domain='real')\n",
    "\n",
    "# Functions to decompose the scalar field\n",
    "psi = M.scalar_field(function('psi')(t,r), name='\\psi')\n",
    "Y = M.scalar_field(function('Y')(th,ph), name='Y')\n",
    "\n",
    "phi = (1/r)*psi*Y\n",
    "\n",
    "KG = nabla(nabla(phi).up(g,0))['^a_a'] - m^2*phi\n",
    "KG = KG.expr()\n",
    "KG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "Let us expand this expression to get some \"intuition\":\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-\\frac{m^{2} Y\\left({\\theta}, {\\phi}\\right) \\psi\\left(t, r\\right)}{r} + \\frac{Y\\left({\\theta}, {\\phi}\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right) \\frac{\\partial}{\\partial r}\\psi\\left(t, r\\right)}{r} + \\frac{Y\\left({\\theta}, {\\phi}\\right) f\\left(r\\right) \\frac{\\partial^{2}}{(\\partial r)^{2}}\\psi\\left(t, r\\right)}{r} - \\frac{Y\\left({\\theta}, {\\phi}\\right) \\psi\\left(t, r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}} - \\frac{Y\\left({\\theta}, {\\phi}\\right) \\frac{\\partial^{2}}{(\\partial t)^{2}}\\psi\\left(t, r\\right)}{r f\\left(r\\right)} + \\frac{\\cos\\left({\\theta}\\right) \\psi\\left(t, r\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} \\sin\\left({\\theta}\\right)} + \\frac{\\psi\\left(t, r\\right) \\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3}} + \\frac{\\psi\\left(t, r\\right) \\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} \\sin\\left({\\theta}\\right)^{2}}</script></html>"
      ],
      "text/plain": [
       "-m^2*Y(th, ph)*psi(t, r)/r + Y(th, ph)*diff(f(r), r)*diff(psi(t, r), r)/r + Y(th, ph)*f(r)*diff(psi(t, r), r, r)/r - Y(th, ph)*psi(t, r)*diff(f(r), r)/r^2 - Y(th, ph)*diff(psi(t, r), t, t)/(r*f(r)) + cos(th)*psi(t, r)*diff(Y(th, ph), th)/(r^3*sin(th)) + psi(t, r)*diff(Y(th, ph), th, th)/r^3 + psi(t, r)*diff(Y(th, ph), ph, ph)/(r^3*sin(th)^2)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KG.expand()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "From this we can see that all terms contain $Y(\\theta,\\phi)$. Since this expression is the left-hand-side of the equation, we can divide it by $Y$ with no remorse:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-\\frac{m^{2} \\psi\\left(t, r\\right)}{r} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right) \\frac{\\partial}{\\partial r}\\psi\\left(t, r\\right)}{r} + \\frac{f\\left(r\\right) \\frac{\\partial^{2}}{(\\partial r)^{2}}\\psi\\left(t, r\\right)}{r} - \\frac{\\psi\\left(t, r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}} - \\frac{\\frac{\\partial^{2}}{(\\partial t)^{2}}\\psi\\left(t, r\\right)}{r f\\left(r\\right)} + \\frac{\\cos\\left({\\theta}\\right) \\psi\\left(t, r\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)} + \\frac{\\psi\\left(t, r\\right) \\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right)} + \\frac{\\psi\\left(t, r\\right) \\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)^{2}}</script></html>"
      ],
      "text/plain": [
       "-m^2*psi(t, r)/r + diff(f(r), r)*diff(psi(t, r), r)/r + f(r)*diff(psi(t, r), r, r)/r - psi(t, r)*diff(f(r), r)/r^2 - diff(psi(t, r), t, t)/(r*f(r)) + cos(th)*psi(t, r)*diff(Y(th, ph), th)/(r^3*Y(th, ph)*sin(th)) + psi(t, r)*diff(Y(th, ph), th, th)/(r^3*Y(th, ph)) + psi(t, r)*diff(Y(th, ph), ph, ph)/(r^3*Y(th, ph)*sin(th)^2)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KG = (KG/Y.expr()).expand()\n",
    "KG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "Now we can see that at least the five terms contain the function $\\psi$ with no derivatives. Let us collect them:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{\\left(\\frac{m^{2}}{r} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}} - \\frac{\\cos\\left({\\theta}\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)} - \\frac{\\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right)} - \\frac{\\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)^{2}}\\right)} \\psi\\left(t, r\\right) + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right) \\frac{\\partial}{\\partial r}\\psi\\left(t, r\\right)}{r} + \\frac{f\\left(r\\right) \\frac{\\partial^{2}}{(\\partial r)^{2}}\\psi\\left(t, r\\right)}{r} - \\frac{\\frac{\\partial^{2}}{(\\partial t)^{2}}\\psi\\left(t, r\\right)}{r f\\left(r\\right)}</script></html>"
      ],
      "text/plain": [
       "-(m^2/r + diff(f(r), r)/r^2 - cos(th)*diff(Y(th, ph), th)/(r^3*Y(th, ph)*sin(th)) - diff(Y(th, ph), th, th)/(r^3*Y(th, ph)) - diff(Y(th, ph), ph, ph)/(r^3*Y(th, ph)*sin(th)^2))*psi(t, r) + diff(f(r), r)*diff(psi(t, r), r)/r + f(r)*diff(psi(t, r), r, r)/r - diff(psi(t, r), t, t)/(r*f(r))"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KG = KG.collect(psi.expr())\n",
    "KG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h3>Getting rid of the angular part</h3>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "Let us extract the terms multiplying $\\psi$ in the last expression:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-\\frac{m^{2}}{r} - \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}} + \\frac{\\cos\\left({\\theta}\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)} + \\frac{\\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right)} + \\frac{\\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{r^{3} Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)^{2}}</script></html>"
      ],
      "text/plain": [
       "-m^2/r - diff(f(r), r)/r^2 + cos(th)*diff(Y(th, ph), th)/(r^3*Y(th, ph)*sin(th)) + diff(Y(th, ph), th, th)/(r^3*Y(th, ph)) + diff(Y(th, ph), ph, ph)/(r^3*Y(th, ph)*sin(th)^2)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "psiPart = KG.coefficient(psi.expr())\n",
    "psiPart"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "This expression can be separated into a part which does not depend on the angular coordinates and other that depends (notice that the purely angular part is rescaled by a factor $1/r^3$)\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-\\frac{m^{2}}{r} - \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}}</script></html>"
      ],
      "text/plain": [
       "-m^2/r - diff(f(r), r)/r^2"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "radialPart = psiPart.operator()(*psiPart.operands()[:2])\n",
    "radialPart"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\cos\\left({\\theta}\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right)}{Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)} + \\frac{\\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{Y\\left({\\theta}, {\\phi}\\right)} + \\frac{\\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)^{2}}</script></html>"
      ],
      "text/plain": [
       "cos(th)*diff(Y(th, ph), th)/(Y(th, ph)*sin(th)) + diff(Y(th, ph), th, th)/Y(th, ph) + diff(Y(th, ph), ph, ph)/(Y(th, ph)*sin(th)^2)"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "angularPart = psiPart.operator()(*psiPart.operands()[2:])\n",
    "angularPart = (r^3*angularPart).expand() # Remove the scale factor\n",
    "angularPart"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "Now let us compute the angular part of the laplacian, written in spherical coordinates:\n",
    "\\begin{equation}\n",
    "\\Delta_\\Omega := \\frac{1}{\\sin^2\\theta}\\frac{\\partial^2}{\\partial\\phi^2} + \\frac{1}{\\sin\\theta}\\frac{\\partial}{\\partial\\theta}\\bigg(\\sin\\theta\\frac{\\partial}{\\partial\\theta}\\bigg) \n",
    "\\end{equation}\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\frac{\\cos\\left({\\theta}\\right) \\frac{\\partial}{\\partial {\\theta}}Y\\left({\\theta}, {\\phi}\\right)}{Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)} + \\frac{\\frac{\\partial^{2}}{(\\partial {\\theta})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{Y\\left({\\theta}, {\\phi}\\right)} + \\frac{\\frac{\\partial^{2}}{(\\partial {\\phi})^{2}}Y\\left({\\theta}, {\\phi}\\right)}{Y\\left({\\theta}, {\\phi}\\right) \\sin\\left({\\theta}\\right)^{2}}</script></html>"
      ],
      "text/plain": [
       "cos(th)*diff(Y(th, ph), th)/(Y(th, ph)*sin(th)) + diff(Y(th, ph), th, th)/Y(th, ph) + diff(Y(th, ph), ph, ph)/(Y(th, ph)*sin(th)^2)"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "DeltaOmega = (1/sin(th)^2)*diff(Y.expr(), ph, 2) + (1/sin(th))*diff(sin(th)*diff(Y.expr(),th),th)\n",
    "(DeltaOmega/Y.expr()).expand()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Visually it is clear that this expression is equal to the angular part of our equation, but we can easily check that this is indeed the angular part:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}0</script></html>"
      ],
      "text/plain": [
       "0"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(DeltaOmega/Y.expr()).expand() - angularPart"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "Therefore, the angular part of the differential equation is\n",
    "\\begin{equation}\n",
    "\\frac{1}{r^3}\\frac{\\Delta_\\Omega Y(\\theta,\\phi)}{Y(\\theta,\\phi)}.\n",
    "\\end{equation}\n",
    "If $Y$ are the spherical harmonics we have\n",
    "\\begin{equation}\n",
    "\\Delta_\\Omega Y = -\\ell(\\ell + 1) Y.\n",
    "\\end{equation}\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{\\left(\\frac{m^{2}}{r} + \\frac{{\\left({\\ell} + 1\\right)} {\\ell}}{r^{3}} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}}\\right)} \\psi\\left(t, r\\right) + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right) \\frac{\\partial}{\\partial r}\\psi\\left(t, r\\right)}{r} + \\frac{f\\left(r\\right) \\frac{\\partial^{2}}{(\\partial r)^{2}}\\psi\\left(t, r\\right)}{r} - \\frac{\\frac{\\partial^{2}}{(\\partial t)^{2}}\\psi\\left(t, r\\right)}{r f\\left(r\\right)}</script></html>"
      ],
      "text/plain": [
       "-(m^2/r + (l + 1)*l/r^3 + diff(f(r), r)/r^2)*psi(t, r) + diff(f(r), r)*diff(psi(t, r), r)/r + f(r)*diff(psi(t, r), r, r)/r - diff(psi(t, r), t, t)/(r*f(r))"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l = var('l', latex_name='\\ell', domain='integer')\n",
    "tmp = KG.operands() # Split the differential equation terms\n",
    "tmp[0] = (radialPart - (l*(l+1))/r^3)*psi.expr() # The first term is the $\\psi$ term\n",
    "KG = KG.operator()(*tmp)\n",
    "KG = KG.collect(psi.expr())\n",
    "KG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<h3>Changing to tortoise coordinate</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\Psi</script></html>"
      ],
      "text/plain": [
       "Scalar field \\Psi on the 4-dimensional differentiable manifold M"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = var('x', domain='real')\n",
    "Psi = M.scalar_field(function('Psi')(t,x), name='\\Psi')\n",
    "Psi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{\\left(\\frac{m^{2}}{r} + \\frac{{\\left({\\ell} + 1\\right)} {\\ell}}{r^{3}} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r^{2}}\\right)} \\Psi\\left(t, x\\right) - \\frac{{\\left(\\frac{\\frac{\\partial}{\\partial x}\\Psi\\left(t, x\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{f\\left(r\\right)^{2}} - \\frac{\\frac{\\partial^{2}}{(\\partial x)^{2}}\\Psi\\left(t, x\\right)}{f\\left(r\\right)^{2}}\\right)} f\\left(r\\right)}{r} + \\frac{\\frac{\\partial}{\\partial x}\\Psi\\left(t, x\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{r f\\left(r\\right)} - \\frac{\\frac{\\partial^{2}}{(\\partial t)^{2}}\\Psi\\left(t, x\\right)}{r f\\left(r\\right)}</script></html>"
      ],
      "text/plain": [
       "-(m^2/r + (l + 1)*l/r^3 + diff(f(r), r)/r^2)*Psi(t, x) - (diff(Psi(t, x), x)*diff(f(r), r)/f(r)^2 - diff(Psi(t, x), x, x)/f(r)^2)*f(r)/r + diff(Psi(t, x), x)*diff(f(r), r)/(r*f(r)) - diff(Psi(t, x), t, t)/(r*f(r))"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KGtmp = KG.subs({psi.expr():Psi.expr(),\\\n",
    "               diff(psi.expr(), t, 2):diff(Psi.expr(), t, 2), \\\n",
    "               diff(psi.expr(), r):(1/f.expr())*diff(Psi.expr(), x), \\\n",
    "               diff(psi.expr(), r, 2):-(diff(f.expr(),r)/f.expr()^2)*diff(Psi.expr(), x) \\\n",
    "               + diff(Psi.expr(),x,2)/f.expr()^2})\n",
    "KGtmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "Now we multiply all terms by $r f(r)$:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-m^{2} \\Psi\\left(t, x\\right) f\\left(r\\right) - \\frac{{\\ell}^{2} \\Psi\\left(t, x\\right) f\\left(r\\right)}{r^{2}} - \\frac{\\Psi\\left(t, x\\right) f\\left(r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{r} - \\frac{{\\ell} \\Psi\\left(t, x\\right) f\\left(r\\right)}{r^{2}} - \\frac{\\partial^{2}}{(\\partial t)^{2}}\\Psi\\left(t, x\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}\\Psi\\left(t, x\\right)</script></html>"
      ],
      "text/plain": [
       "-m^2*Psi(t, x)*f(r) - l^2*Psi(t, x)*f(r)/r^2 - Psi(t, x)*f(r)*diff(f(r), r)/r - l*Psi(t, x)*f(r)/r^2 - diff(Psi(t, x), t, t) + diff(Psi(t, x), x, x)"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KGtmp = (r*f.expr()*KGtmp).expand()\n",
    "KGtmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "We now group the terms containing $\\Psi$\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{\\left(m^{2} f\\left(r\\right) + \\frac{{\\ell}^{2} f\\left(r\\right)}{r^{2}} + \\frac{f\\left(r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{r} + \\frac{{\\ell} f\\left(r\\right)}{r^{2}}\\right)} \\Psi\\left(t, x\\right) - \\frac{\\partial^{2}}{(\\partial t)^{2}}\\Psi\\left(t, x\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}\\Psi\\left(t, x\\right)</script></html>"
      ],
      "text/plain": [
       "-(m^2*f(r) + l^2*f(r)/r^2 + f(r)*diff(f(r), r)/r + l*f(r)/r^2)*Psi(t, x) - diff(Psi(t, x), t, t) + diff(Psi(t, x), x, x)"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KGtmp = KGtmp.expand().collect(Psi.expr())\n",
    "KGtmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "The term between parenthesis can be written in a more \"pleasant\" form. We first split the whole expression into operands,\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left[-{\\left(m^{2} f\\left(r\\right) + \\frac{{\\ell}^{2} f\\left(r\\right)}{r^{2}} + \\frac{f\\left(r\\right) \\frac{\\partial}{\\partial r}f\\left(r\\right)}{r} + \\frac{{\\ell} f\\left(r\\right)}{r^{2}}\\right)} \\Psi\\left(t, x\\right), -\\frac{\\partial^{2}}{(\\partial t)^{2}}\\Psi\\left(t, x\\right), \\frac{\\partial^{2}}{(\\partial x)^{2}}\\Psi\\left(t, x\\right)\\right]</script></html>"
      ],
      "text/plain": [
       "[-(m^2*f(r) + l^2*f(r)/r^2 + f(r)*diff(f(r), r)/r + l*f(r)/r^2)*Psi(t, x),\n",
       " -diff(Psi(t, x), t, t),\n",
       " diff(Psi(t, x), x, x)]"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tmp = KGtmp.operands()\n",
    "tmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "And then we substitute \"by hand\" the first element of the list:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}\\left[-{\\left(m^{2} + \\frac{{\\left({\\ell} + 1\\right)} {\\ell}}{r^{2}} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r}\\right)} \\Psi\\left(t, x\\right) f\\left(r\\right), -\\frac{\\partial^{2}}{(\\partial t)^{2}}\\Psi\\left(t, x\\right), \\frac{\\partial^{2}}{(\\partial x)^{2}}\\Psi\\left(t, x\\right)\\right]</script></html>"
      ],
      "text/plain": [
       "[-(m^2 + (l + 1)*l/r^2 + diff(f(r), r)/r)*Psi(t, x)*f(r),\n",
       " -diff(Psi(t, x), t, t),\n",
       " diff(Psi(t, x), x, x)]"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tmp[0] = -f.expr()*(m^2 + diff(f.expr(),r)/r + (l*(l+1))/r^2)*Psi.expr()\n",
    "tmp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "Now we finally group everything together and obtain the Klein-Gordon equation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{\\left(m^{2} + \\frac{{\\left({\\ell} + 1\\right)} {\\ell}}{r^{2}} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r}\\right)} \\Psi\\left(t, x\\right) f\\left(r\\right) - \\frac{\\partial^{2}}{(\\partial t)^{2}}\\Psi\\left(t, x\\right) + \\frac{\\partial^{2}}{(\\partial x)^{2}}\\Psi\\left(t, x\\right)</script></html>"
      ],
      "text/plain": [
       "-(m^2 + (l + 1)*l/r^2 + diff(f(r), r)/r)*Psi(t, x)*f(r) - diff(Psi(t, x), t, t) + diff(Psi(t, x), x, x)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "KG = KGtmp.operator()(*tmp)\n",
    "KG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "The term multiplying $\\Psi$ is the scattering potential:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}-{\\left(m^{2} + \\frac{{\\left({\\ell} + 1\\right)} {\\ell}}{r^{2}} + \\frac{\\frac{\\partial}{\\partial r}f\\left(r\\right)}{r}\\right)} f\\left(r\\right)</script></html>"
      ],
      "text/plain": [
       "-(m^2 + (l + 1)*l/r^2 + diff(f(r), r)/r)*f(r)"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "RWpotential = KG.coefficient(Psi.expr())\n",
    "RWpotential"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "<p>\n",
    "We can check that this potential is in agreement with Schwarzschild case:\n",
    "</p>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<html><script type=\"math/tex; mode=display\">\\newcommand{\\Bold}[1]{\\mathbf{#1}}{\\left(m^{2} + \\frac{{\\left({\\ell} + 1\\right)} {\\ell}}{r^{2}} + \\frac{2 \\, M}{r^{3}}\\right)} {\\left(\\frac{2 \\, M}{r} - 1\\right)}</script></html>"
      ],
      "text/plain": [
       "(m^2 + (l + 1)*l/r^2 + 2*M/r^3)*(2*M/r - 1)"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "M = var('M')\n",
    "RWpotential.subs({f.expr():1-2*M/r, diff(f.expr(),r):2*M/r^2})"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "SageMath 7.6",
   "language": "",
   "name": "sagemath"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
