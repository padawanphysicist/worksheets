\documentclass[10pt]{article}
\usepackage[scale=.8]{geometry}
\usepackage[fleqn]{amsmath}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{hyperref}
%\usepackage{inconsolata}
\usepackage{color}
\usepackage{tableaux}
\usepackage{breqn}
\newcommand{\algorithm}[2]{{\tt\Large\detokenize{#1}}\\[1ex]
{\emph{#2}}\\[-1ex]
}\newcommand{\property}[2]{{\tt\Large\detokenize{#1}}\\[1ex]
{\emph{#2}}\\[-1ex]
}\newcommand{\algo}[1]{{\tt #1}}
\newcommand{\prop}[1]{{\tt #1}}
\setlength{\mathindent}{1em}
\lstnewenvironment{python}[1][]
{\lstset{language=Python, columns=fullflexible, xleftmargin=1em, basicstyle=\small\ttfamily\color{blue}, keywordstyle={}}}{}
\begin{document}
\begin{center}
{\Large\bf Klein-Gordon equation}

Victor Santos\\
\texttt{victor\_santos@fisica.ufc.br}\\
\today

This is a Cadabra worksheet to derive the equation of motion for a \href{https://en.wikipedia.org/wiki/Scalar_field_theory}{real scalar field} $\phi(x)$ in a curved spacetime.

This worksheet uses \textcolor{blue}{Cadabra 2.1.2}
\end{center}
Before we define the action, we declare some symbols and their properties. We first declare the indices, $x$ as a coordinate and $\nabla$ as a derivative:
\begin{python}
{a,b,c,d}::Indices(position=free).
x::Coordinate.
\nabla{#}::Derivative.
\end{python}
We now declare the scalar field $\phi$ and the variation symbol $\delta$:
\begin{python}
\phi::Depends(x,\nabla{#}).
\delta{#}::Accent.
\bar{#}::Accent.
\end{python}
We are now in position to define the action. The lagrangian for a real scalar field is given by
\begin{equation}
L = -\frac{1}{2}\big[\nabla_{a}{\phi}\nabla^{a}{\phi} + V(\phi)\big].
\end{equation}
With two real scalar fields $\phi_{1}$ and $\phi_{2}$ we can construct the complex scalar fields
\begin{align}
\phi &= \frac{\phi_1 + \mathrm{i}\phi_2}{\sqrt{2}}\\
\bar{\phi} &= \frac{\phi_1 - \mathrm{i}\phi_2}{\sqrt{2}}.
\end{align}
and the combine action can be written
\begin{align}
L &= -\frac{1}{2}\big[\nabla_{a}{\phi_{1}}\nabla^{a}{\phi_{1}} + U_1(\phi_1)\big]-\frac{1}{2}\big[\nabla_{a}{\phi_{2}}\nabla^{a}{\phi_{2}} + U_2(\phi_2)\big]\\
&= -\big[\nabla_{a}{\bar{\phi}}\nabla^{a}{\phi} + U(\phi,\bar{\phi})\big],\ U(\phi,\bar{\phi}) = \frac{1}{2}\big[U_1(\phi_1) + U_2(\phi_2)\big].
\end{align}
However, imposing the condition that the lagrangian must be invariant under $U(1)$ transformations restrict the potential to the form $U(\phi,\bar{\phi})=U(\phi\bar{\phi})$,
and then we have simply
\begin{equation}
L = -\big[\nabla_{a}{\bar{\phi}}\nabla^{a}{\phi} + U(\phi\bar{\phi})\big]
\end{equation}
\begin{python}
S:= -\int{ \sqrt{-g} (\nabla_{a}{\bar{\phi}} \nabla^{a}{\phi} + V) }{x};
\end{python}
\begin{dmath*}{}-\int{}\sqrt{-g} \left(\nabla_{a}\left(\bar{\phi}\right) \nabla^{a}{\phi}+V\right)\, {\rm d}x\end{dmath*}
So far (version 2.1.2) Cadabra does not have a way to handle and object like $V$ to depend on other objects like $\phi$; therefore we must add a rule of how $V$ is supposed to vary:
\begin{python}
variationRules:= \phi -> \delta{\phi}, V -> V' \bar{\phi} \delta{\phi} + V' \phi \delta{\bar{\phi}};
\end{python}
\begin{dmath*}{}\left\{\phi \rightarrow \delta{\phi},~\linebreak[0] V \rightarrow V' \bar{\phi} \delta{\phi}+V' \phi \delta{\bar{\phi}}\right\}\end{dmath*}
We now vary the action $S$ with respect to $\phi$:
\begin{python}
vary(S, variationRules);
\end{python}
\begin{dmath*}{}-\int{}\sqrt{-g} \left(\nabla_{a}\left(\bar{\delta{\phi}}\right) \nabla^{a}{\phi}+\nabla_{a}\left(\bar{\phi}\right) \nabla^{a}{\delta{\phi}}+V' \bar{\phi} \delta{\phi}+V' \phi \delta{\bar{\phi}}\right)\, {\rm d}x\end{dmath*}
\begin{python}
sort_product(S)
canonicalise(S);
\end{python}
\begin{dmath*}{}-\int{}\sqrt{-g} \left(\nabla^{a}\left(\bar{\delta{\phi}}\right) \nabla_{a}{\phi}+\nabla^{a}\left(\bar{\phi}\right) \nabla_{a}{\delta{\phi}}+V' \bar{\phi} \delta{\phi}+V' \delta{\bar{\phi}} \phi\right)\, {\rm d}x\end{dmath*}
We now distribute the terms containing $\sqrt{-g}$,
\begin{python}
distribute(S);
\end{python}
\begin{dmath*}{}-\int{}\left(\sqrt{-g} \nabla^{a}\left(\bar{\delta{\phi}}\right) \nabla_{a}{\phi}+\sqrt{-g} \nabla^{a}\left(\bar{\phi}\right) \nabla_{a}{\delta{\phi}}+\sqrt{-g} V' \bar{\phi} \delta{\phi}+\sqrt{-g} V' \delta{\bar{\phi}} \phi\right)\, {\rm d}x\end{dmath*}
so that we can integrate by parts and remove the terms $\nabla^{a}\delta\phi$:
\begin{python}
integrate_by_parts(S, $\delta{\phi}$);
\end{python}
\begin{dmath*}{}-\int{}\left(-\nabla^{a}\left(\sqrt{-g}\right) \bar{\delta{\phi}} \nabla_{a}{\phi}-\sqrt{-g} \bar{\delta{\phi}} \nabla^{a}\left(\nabla_{a}{\phi}\right)-\nabla_{a}\left(\sqrt{-g} \nabla^{a}\left(\bar{\phi}\right)\right) \delta{\phi}+\sqrt{-g} V' \bar{\phi} \delta{\phi}+\sqrt{-g} V' \delta{\bar{\phi}} \phi\right)\, {\rm d}x\end{dmath*}
\begin{python}
product_rule(S);
\end{python}
\begin{dmath*}{}-\int{}\left(-\nabla^{a}\left(\sqrt{-g}\right) \bar{\delta{\phi}} \nabla_{a}{\phi}-\sqrt{-g} \bar{\delta{\phi}} \nabla^{a}\left(\nabla_{a}{\phi}\right)-\left(\nabla_{a}\left(\sqrt{-g}\right) \nabla^{a}\left(\bar{\phi}\right)+\sqrt{-g} \nabla_{a}\left(\nabla^{a}\left(\bar{\phi}\right)\right)\right) \delta{\phi}+\sqrt{-g} V' \bar{\phi} \delta{\phi}+\sqrt{-g} V' \delta{\bar{\phi}} \phi\right)\, {\rm d}x\end{dmath*}
Assuming a metric-compatible derivative, the terms $\nabla^a(\sqrt{-g})$ vanish:
\begin{python}
substitute(S, $\nabla_{a}{\sqrt{-g}} -> 0$);
\end{python}
\begin{dmath*}{}-\int{}\left(-\sqrt{-g} \bar{\delta{\phi}} \nabla^{a}\left(\nabla_{a}{\phi}\right)-\sqrt{-g} \nabla_{a}\left(\nabla^{a}\left(\bar{\phi}\right)\right) \delta{\phi}+\sqrt{-g} V' \bar{\phi} \delta{\phi}+\sqrt{-g} V' \delta{\bar{\phi}} \phi\right)\, {\rm d}x\end{dmath*}
\begin{python}
factor_out(S, $\sqrt{-g}$)
factor_out(S, $\delta{\phi}$);
\end{python}
\begin{dmath*}{}-\int{}\sqrt{-g} \left(-\bar{\delta{\phi}} \nabla^{a}\left(\nabla_{a}{\phi}\right)+V' \delta{\bar{\phi}} \phi+\delta{\phi} \left(-\nabla_{a}\left(\nabla^{a}\left(\bar{\phi}\right)\right)+V' \bar{\phi}\right)\right)\, {\rm d}x\end{dmath*}
With this we have the equations of motion
\begin{align}
\nabla^{a}\nabla_{a}\phi - V'(\phi\bar{\phi})\,\phi &= 0,\\
\nabla^{a}\nabla_{a}\bar{\phi} - V'(\phi\bar{\phi})\,\bar{\phi} &= 0.
\end{align}
Observe that in the particular case there $\phi$ is real, we recover the single equation 
\begin{equation}
\nabla^{a}\nabla_{a}\phi - V'(\phi^2)\,\phi = 0.
\end{equation}
The difference between this equation and the "usual" Klein-Gordon equation is the dependency of the potential, which is now a function of the square of the field. 
Therefore, the quadratic potential $m\phi^2$ is written as a \emph{linear} function of $\phi^2$.
\begin{python}
\end{python}
\end{document}
